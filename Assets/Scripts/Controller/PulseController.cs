﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PulseController : MonoBehaviour {

    public SteamVR_Action_Vibration haptic;
    public bool coroutineStart = false;

    private IEnumerator m_controllerHaptic; // Coroutine qui gère les vibrations de la manette

    private void Start()
    {
        m_controllerHaptic = PulseHaptic(300, 300);
    }

    IEnumerator PulseHaptic(float frequency, float amplitude)
    {

        haptic.Execute(0, 0.1f, 300, 300, SteamVR_Input_Sources.LeftHand);
        haptic.Execute(0, 0.1f, 300, 300, SteamVR_Input_Sources.RightHand);

        

        yield return new WaitForSeconds(0.1f);
        if (coroutineStart == true)
        {
            StartCoroutine(m_controllerHaptic);
        }
        
    }

    public void StartPulse()
    {
        coroutineStart = true;
        StartCoroutine(m_controllerHaptic);
    }

    public void StopPulse()
    {
        if(coroutineStart)
        {
            coroutineStart = false;
            StopCoroutine(m_controllerHaptic);
        }
       
    }
}

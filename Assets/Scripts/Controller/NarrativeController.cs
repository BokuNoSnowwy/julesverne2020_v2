﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeController : MonoBehaviour {

    [Header("Audio Control")]
    public List<AudioClip> clipVoixOff;
    public AudioSource sourceVoix;

    [Header("Event")]
    public List<Event> events;

    [Header("States")]
    public bool wait;

    private int m_indexClip = -1;


    // Use this for initialization
    void Start () {

	}

    private void Update()
    {
        if(sourceVoix.isPlaying)
        {
            wait = true;
        }
        else
        {
            wait = false;
        }
    }

    public void NextClip()
    {
        ++m_indexClip;

        if(m_indexClip < clipVoixOff.Count)
        {
            sourceVoix.clip = clipVoixOff[m_indexClip];
            sourceVoix.Play();
        }
    }

    public void PassClip()
    {
        ++m_indexClip;
    }

    public void ReturnFirstClip()
    {
        m_indexClip = -1;
    }

    public float GetClipLenght()
    {
        return sourceVoix.clip.length;
    }

    public void RandomClip()
    {
        Random.InitState((int)Time.time);
        int randomIndex = Random.Range(0, clipVoixOff.Count);
        sourceVoix.clip = clipVoixOff[randomIndex];
        sourceVoix.Play();
    }


}


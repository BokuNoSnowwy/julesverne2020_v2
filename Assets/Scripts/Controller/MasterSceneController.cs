﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

/// <summary>
/// Gère le chargement et le déchargement des différentes scènes. Afin d'éviter des problèmes de transition via la scène par défault de SteamVR, l'ensemble des scènes est load au démarrage de l'application puis caché via
/// un GameObject root. A chaque changement de scène il faut donc activer la scène et afficher le GameObject Root. Une amélioration possible est d'utilise le Load de scène asynchrone avec le nouveau Job System de Unity.
/// </summary>
public class MasterSceneController : MonoBehaviour {

    public GameObject teleport;
    public static MasterSceneController Instance { private set; get; } // Le singleton
    public string[] allSceneName; // On stock le nom des scènes

    public List<GameObject> listIdea; // On garde une trace des idées mondes restantes (permet d'éviter que des idées mondes réapparaissent dans la MainScene)

    [HideInInspector]
    public bool loadingFinished = false; // Détermine si le load des scènes et terminé

	public bool mainSceneActive = false; 
    public bool alreadyPassMainScene = false; // On stock le fait qu'un utilisateur est déjà passé par la main scene pour éviter de relancer l'audio

    private SceneLoader m_SceneLoader; // Système qui gère le chargement/déchargement des scènes
    private CameraLoader m_CameraLoader; // Idem mais pour les caméra. Je ne sais pas pourquoi, mais on doit charger et décharger les caméras sinon ça provoque des bugs.
    private DecorManagerMain decor;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
		m_SceneLoader = GetComponent<SceneLoader>();
        m_CameraLoader = GetComponent<CameraLoader>();
    }
	
	// Update is called once per frame
	void Update () {
		
        // On check la fin du chargement des scènes
        loadingFinished = (m_SceneLoader.nbSceneLoad == allSceneName.Length);

        if (Input.GetKeyDown(KeyCode.T))
        {
            MasterSceneController.Instance.LoadScene("MainScene", Color.white);
            decor.ResetThis();
        }
	}

    public void Init()
    {
        Debug.Log("chargement des scènes du jeu");
        m_SceneLoader.InitialiseSceneAsync(allSceneName);
    }

    /// <summary>
    /// Load une scène.
    /// </summary>
    /// <param name="sceneName">Le nom de la scène</param>
    /// <param name="fadeColor">La couleur du fade</param>
    public void LoadScene(string sceneName, Color fadeColor)
    {
        StartCoroutine(CO_LoadScene(sceneName, fadeColor));
    }

    /// <summary>
    /// Load une caméra dans la scène
    /// </summary>
    /// <param name="indexCam">L'index de la caméra dans le tableau. Il y en as deux différentes (Player et [CameraRig] de SteamVR)</param>
    /// <param name="indexPostProcess">Le profile post processing à associé à la caméra</param>
    /// <param name="position">La position de départ de la caméra</param>
    /// <param name="rotation">La rotation de départ de la caméra</param>
    public void LoadCamera(int indexCam, int indexPostProcess,Vector3 position, Vector3 rotation)
    {
        //m_CameraLoader.LoadCamera(indexCam, indexPostProcess, position, rotation, m_SceneLoader.actualRoot.transform);
    }

    IEnumerator CO_LoadScene(string sceneName, Color fadeColor)
    {
        teleport.SetActive(false);
        SteamVR_Fade.Start(Color.clear, 0);
        SteamVR_Fade.Start(fadeColor,4);
        yield return new WaitForSeconds(2);
        //m_CameraLoader.UnloadCamera(); // On unload la caméra d'abord.
        m_SceneLoader.ActiveScene(sceneName, fadeColor); // Ensuite on active la scène
    }

    public void GetMain(DecorManagerMain potato)
    {
        if(decor==null)
        decor = potato;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

/// <summary>
/// Gère le chargement/déchargement des caméras. Bizzarement, la camera [CameraRig] de SteamVR refuse de fonctionner quand une caméra Player a été activé avant. Pour éviter se problème on doit charger et décharger les 
/// caméras à chaque fois qu'on change de scène. 
/// </summary>
public class CameraLoader : MonoBehaviour {

    public List<GameObject> vrCameras; // La liste des caméras

    public List<PostProcessingProfile> postProcessCamera; // la liste des profiles de post processing

    public GameObject cameraLoaded; // On stock la caméra chargé afin de pouvoir la déchargé par la suite

    /// <summary>
    /// Load une caméra dans la scène
    /// </summary>
    /// <param name="indexCam">L'index de la caméra dans le tableau. Il y en as deux différentes (Player et [CameraRig] de SteamVR)</param>
    /// <param name="indexPostProcess">Le profile post processing à associé à la caméra</param>
    /// <param name="position">La position de départ de la caméra</param>
    /// <param name="rotation">La rotation de départ de la caméra</param>
    public void LoadCamera(int indexCam, int indexPostProcess, Vector3 position, Vector3 rotation,Transform parent)
    {
        GameObject vrCamera = vrCameras[indexCam];
        cameraLoaded = Instantiate(vrCamera, position, Quaternion.Euler(rotation), parent);
        //cameraLoaded.GetComponent<LinkPostProcess>().postProcessBehave.profile = postProcessCamera[indexPostProcess];
    }

    /// <summary>
    /// Unload la caméra stocké dans cameraLoaded. La fonction ne fais juste que Destroy le gameObject associé à la caméra.
    /// </summary>
    public void UnloadCamera()
    {
        Destroy(cameraLoaded);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PointerController : MonoBehaviour
{
    public float DefaultLength = 5.0f;
    public GameObject Dot;
    public VRInputModule m_InputModule;
    
    private LineRenderer m_LineRenderer = null;
    
    private void Awake()
    {
        m_LineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLine();
    }

    private void UpdateLine()
    {
        // define distance
        PointerEventData data = m_InputModule.GetData();
        float targetlength = data.pointerCurrentRaycast.distance == 0 ? DefaultLength : data.pointerCurrentRaycast.distance;

        //raycast
        RaycastHit Hit = CreateRayCast(targetlength);

        //default
        Vector3 endPosition = transform.position - (transform.forward * targetlength);

        // on hit
        if (Hit.collider != null)
        {
            endPosition = Hit.point;
        }

        // set position dot
        Dot.transform.position = endPosition;

        //line renderer
        m_LineRenderer.SetPosition(0,transform.position);
        m_LineRenderer.SetPosition(1,endPosition);
    }
    
    private RaycastHit CreateRayCast(float length)
    {
        RaycastHit Hit;
        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out Hit, DefaultLength);
        
        return Hit;
    }
}

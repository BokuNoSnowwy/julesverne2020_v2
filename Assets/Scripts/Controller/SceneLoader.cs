﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;

/// <summary>
/// Gère le chargement et le déchargement des scènes.
/// </summary>
public class SceneLoader : MonoBehaviour
{
    public GameObject teleport;
    public int nbSceneLoad = 0; // On stock le nombre de scène load pour ensuite déterminer si on a bien load toutes les scènes.

    public List<GameObject> rootObjects; // La liste des GameObjects Root qui permet de cacher/afficher une scène. Se remplit en runtime !
    public GameObject rootTuto;


    public GameObject actualRoot; // On stock le GameObject Root de la scène chargée pour ensuite pouvoir le cacher à nouveau.

    public GameObject _player;
    public Transform spawnTuto;
    public GameObject[] cameras;

    private Player _playerHMD;

    /// <summary>
    /// Démarre une coroutine pour load chaque scène de manière asynchrone.
    /// </summary>
    /// <param name="allSceneName">Le tableau comportant le nom de chaque scène à load</param>
    public void InitialiseSceneAsync(string[] allSceneName)
    {
        _playerHMD = FindObjectOfType<Player>();
        foreach(string sceneName in allSceneName)
        {
            StartCoroutine(LoadSceneAsync(sceneName));
        }
    }

    /// <summary>
    /// Coroutine pour Load une scène de manière asynchrone.
    /// </summary>
    /// <param name="sceneName">Le nom de la scène à load</param>
    /// <returns>Null</returns>
    public IEnumerator LoadSceneAsync(string sceneName)
    {
        // On appelle la fonction LoadSceneAsyn de Unity
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        async.allowSceneActivation = true;
        while (!async.isDone)
        {
            Debug.Log("Loading !");
            yield return null;
        }
        Debug.Log("End Loading !");
        Scene s = SceneManager.GetSceneByName(sceneName);
        GameObject[] root = s.GetRootGameObjects();
        
        // On cherche le GameObject root de la scène et on le désactive. On le stock ensuite dans la liste.

        
        root[0].SetActive(false);
        if (root[0])
            rootObjects.Add(root[0]);

        ++nbSceneLoad;



        yield return null;
    }

    /// <summary>
    /// Active une scène et affiche le GameObject Root
    /// </summary>
    /// <param name="sceneName">Le nom de la scène à activer</param>
    /// <param name="fadeColor">La couleur de fade</param>
    public void ActiveScene(string sceneName, Color fadeColor)
    {
        Scene scene = SceneManager.GetSceneByName(sceneName);
        SceneManager.SetActiveScene(scene);
        GameObject root = null;
        if (actualRoot != null) // Avant de changer de scène on cache à nouveau le GameObject Root.
        {
            actualRoot.SetActive(false);
        }

        // On récupère le root de la nouvelle scène à charger et on l'active !
        if(sceneName== "SceneTuto copied")
        {
            root = rootTuto;
        }
        else
        {
            root = rootObjects.Find(x => x.scene == scene);
        }
        
        actualRoot = root;
        root.SetActive(true);
        if(sceneName == "SceneTuto copied")
        {
            _player.transform.position = spawnTuto.position;
        }

        else 
        {
            int i = Random.Range(0, root.GetComponent<SpawnPlayer>().spawnPos.Length);
            _player.transform.position = root.GetComponent<SpawnPlayer>().Spawning(i);
        }

        // Comme on a load toutes les scènes au début de l'expérience, on ne peut pas utiliser les fonctions Start et Awake. On doit donc créer une fonction InitScene pour chaque scène afin de servir de fonction Start.
        if (sceneName != "SceneTuto copied")
        {
            root.GetComponent<IDecorManager>().InitScene(fadeColor);
        }
        else
        {
            root.GetComponentInChildren<TutoController>().Reloaded(fadeColor);
        }

        for(int i=0; i<cameras.Length; i++)
        {
            cameras[i].SetActive(false);
        }
        switch (sceneName)
        {
            case "SceneTuto copied":
                cameras[0].SetActive(true);
                _playerHMD.hmdTransforms[0] = cameras[0].transform;
                break;
            case "MainScene":
                cameras[1].SetActive(true);
                _playerHMD.hmdTransforms[0] = cameras[1].transform;
                break;
            case "SceneDesert":
                cameras[2].SetActive(true);
                _playerHMD.hmdTransforms[0] = cameras[2].transform;
                break;
            case "SceneGrotte":
                cameras[3].SetActive(true);
                _playerHMD.hmdTransforms[0] = cameras[3].transform;
                break;
            case "SceneGlace":
                cameras[4].SetActive(true);
                _playerHMD.hmdTransforms[0] = cameras[4].transform;
                break;
        }
        teleport.SetActive(true);
    }


}

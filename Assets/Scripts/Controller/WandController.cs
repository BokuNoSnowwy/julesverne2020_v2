﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class WandController : MonoBehaviour
{

    //La fixation sur la main
    public GameObject particles;
    public WandController otherHand;
    public GameObject AudioGrab;
    private FixedJoint m_Joint = null;

    public SteamVR_ActionSet m_ActionSet;


    //L'action d'attraper un objet
    public SteamVR_Action_Boolean grabAction = null;
    //La manette
    private SteamVR_Behaviour_Pose m_Pose = null;


    public GameObject _pickup; // L'objet que l'on va grip
    public bool haveObject;

    private float _hapticDuration = 0.2f; // La durée de la vibration de la manette
    private float _timeHaptic = 0; // Chrono pour la vibration 

    /* AJOUT DE PIERRE BIGAUD */

    //public StockGrab stock; // L'idée que l'on peut grab

    //controller

    public InteractionSphere interaction;

    /* /AJOUT DE PIERRE BIGAUD */
    private void Awake()
    {
        m_ActionSet.Activate(SteamVR_Input_Sources.Any, 0, true);
        m_Pose = GetComponent<SteamVR_Behaviour_Pose>();
        m_Joint = GetComponent<FixedJoint>();
        particles.SetActive(false);
        //a_Pose = GetComponent<SteamVR_Action_Pose>();

    }


    // Lorsqu'on entre dans un trigger
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "idea" || collider.gameObject.tag == "button" || collider.gameObject.tag == "mapObject" || collider.gameObject.tag == "buttonElevator")
        {
            if (!haveObject)
            {
                _pickup = collider.gameObject;
            }
        }
        if (collider.gameObject.tag == "shroom")
        {
            collider.gameObject.GetComponent<MushroomLight>().Shroom();
        }
        if (collider.gameObject.tag == "paw")
        {
            collider.gameObject.GetComponent<SphinxPaw>().TouchPaw();
        }
        particles.SetActive(true);
    }

     void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "gachaBall")
        {
            if(!haveObject)
            {
                _pickup = collision.gameObject;
            }

            particles.SetActive(true);
        }
    }

    // Lorsqu'on sort du trigger
    void OnTriggerExit(Collider collider)
    {
        if ((collider.gameObject.tag == "idea" || collider.gameObject.tag == "button") && collider.gameObject == _pickup && haveObject==false)
        {
            _pickup = null;
        }
        /*if (collider.gameObject.tag == "button" && collider.gameObject == _pickup && haveObject == false)
        {
            _pickup = null;
        }*/
    }
    // Update is called once per frame
    void Update()
    {

        if(_pickup != null)
        {
            if(_pickup != otherHand._pickup)
            {
                particles.transform.position = _pickup.transform.position;
            }
            else if(otherHand.particles.activeSelf == false)
            {
                particles.SetActive(true);
            }
            else
            {
                particles.SetActive(false);
            }
            if (!AudioGrab.activeSelf)
            {
                AudioGrab.SetActive(true);
            }
        }
        else
        {
            AudioGrab.SetActive(false);
        }
        if(_pickup==null || haveObject)
        {
            particles.SetActive(false);
        }
        // Si la gachette est enfoncée
        if (grabAction.GetStateDown(m_Pose.inputSource) && _pickup != null && haveObject == false)
        {
            if (_pickup.tag == "button")
            {
                if (_pickup.GetComponent<MenuController>().id == 0)
                {
                    _pickup.GetComponent<MenuController>().Playing();
                }
                else if (_pickup.GetComponent<MenuController>().id == 1)
                {
                    _pickup.GetComponent<MenuController>().Quitting();
                }
                else if (_pickup.GetComponent<MenuController>().id == 2)
                {
                    _pickup.GetComponent<MenuController>().Speedrun();
                }
            }
            if (_pickup.tag == "buttonElevator")
            {
                _pickup.GetComponent<ButtonToPress>().CallElevatorToPlayer();
            }
            if (_pickup.tag == "gachaBall")
            {
                haveObject = true;
                //position de l'objet = position de la main
                _pickup.transform.position = transform.position;
                //Attacher l'objet à la main
                m_Joint.connectedBody = _pickup.GetComponent<Rigidbody>();

                // On empêche la balle de bouger 
                _pickup.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                _pickup.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
                _pickup.GetComponent<Rigidbody>().useGravity = false;
            }
            if (_pickup.tag == "idea")
            {
                if (_pickup.GetComponent<IdeaManager>().activeHand == null)
                {
                    haveObject = true;
                    // On Stop le mouvement de l'idée
                    _pickup.GetComponent<IdeaManager>().StopMove();

                    // On place l'idée sur la main
                    _pickup.transform.position = transform.position;

                    // On attache l'idée sur le Joint
                    m_Joint.connectedBody = _pickup.GetComponent<Rigidbody>();

                    // On empêche l'idée de bouger 
                    _pickup.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                    _pickup.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);

                    _pickup.GetComponent<IdeaManager>().activeHand = this;
                }

                if(otherHand.GetComponent<WandController>().haveObject==false && _pickup.GetComponent<IdeaManager>().sceneToLoad != "")
                {
                    _pickup.GetComponent<IdeaManager>().GetDisable();
                }
            }
            if(_pickup.tag == "mapObject")
            {
                haveObject = true;
                //position de l'objet = position de la main
                _pickup.transform.position = transform.position;
                //Attacher l'objet à la main
                m_Joint.connectedBody = _pickup.GetComponent<Rigidbody>();
            }

        }

        // Si la gachette est relachée
        if (grabAction.GetStateUp(m_Pose.inputSource))
        {
            if (haveObject == true && _pickup != null)
            {
                Debug.Log(gameObject.name);
                // On relance le déplacement de l'idée
                m_Joint.connectedBody = null;
                if (_pickup.tag == "idea")
                {
                    _pickup.GetComponent<IdeaManager>().activeHand = null;
                    // On l'enlève du controller
                    _pickup.GetComponent<IdeaManager>().StartMove();
                }
                //Quand la balle du distributeur est lachée, on remet la gravité car elle a été desactivée précédement
                if (_pickup.tag == "gachaBall")
                {
                    _pickup.GetComponent<Rigidbody>().useGravity = true;
                }
            }

            if(m_Joint.connectedBody == null)
            {
                haveObject = false;
                _pickup = null;
            }

            // Désactive la vibration
            _timeHaptic = 0;
        }
        if (_pickup != null)
        {
            if (_pickup.activeSelf == false)
            {
                if (haveObject == true && _pickup != null)
                {
                    Debug.Log(gameObject.name);
                    // On relance le déplacement de l'idée
                    m_Joint.connectedBody = null;
                    _pickup.GetComponent<IdeaManager>().activeHand = null;
                    // On l'enlève du controller
                    _pickup.GetComponent<IdeaManager>().StartMove();
                }

                _pickup = null;
            }
        
        }
    }
}

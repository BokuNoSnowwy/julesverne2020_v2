﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public int id;
    public TutoController tuto;
    public AudioSource SoundMenu;

    public void Playing()
    {
        SoundMenu.Play();
        tuto.keepTrackOnMenuPlay = true;
    }

    public void Quitting()
    {
        SoundMenu.Play();
        tuto.keepTrackOnMenuQuit = true;
    }

    public void Speedrun()
    {
        SoundMenu.Play();
        tuto.keepTrackOnMenuSpeed = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class TutoController : MonoBehaviour
{
    [Header("Link")]
    public GameObject panelLoading;
    public NarrativeController narrativeCont;
    public GameObject teleportation;
    public GameObject gate;

    [Header("Intro")]
    public GameObject videoIntro;
    public GameObject planeVideo;
    public AudioSource sourceLogo;

    [Header("Interaction")]
    public GameObject ideeMonde1;
    public GameObject ideeMonde2;
    public Hand hand1;
    public Hand hand2;

    [Header("States")]
    public TutoState state;
    public bool ideeMonde1Appear = false;
    public bool ideeMonde1Grap = false;
    public bool ideeMonde2Grap = false;
    public bool fusionIdea = false;
    public bool skipTuto = false;


    //Plusieurs booléens pour vérifier où on en est
    [Header("CheckStates")]
    public bool keepTrackOnIdeeMonde1Appear = true;
    public bool keepTrackOnIdeeMonde1Grap = true;
    public bool keepTrackOnIdeeMonde2Grap = true;
    public bool keepTrackOnMenuPlay = false;
    public bool keepTrackOnMenuQuit = false;
    public bool keepTrackOnMenuSpeed = false;

    //Les endroits où on peut se téléporter
    public GameObject[] _teleportationFloor;

    [Header("PartsTuto")]
    //La partie du root qui se lance après le menu
    public GameObject _tuto;
    //Le menu. Désactivé quand on a choisi une option
    public GameObject _menu;
    //Les particules de la porte
    public GameObject porteParticules;
    //la partie téléportation du tuto. S'active après la fusion
    public GameObject _tutomove;
    //Un gros bloc noir qui permet de cacher des objets sans avoir à les désactiver
    public GameObject _marouflage;
    private MasterSceneController m_masterSceneController;

    private bool m_finishInit = false;
    private float m_timerLogoAudio = 0;
    private Vector3 _gatePosition;
    public GameObject _gateObject;

    [Header("Ui")]
    public GameObject Image_Tuto;
    public GameObject Img_Cercle;
    private Animator AnimTuto;


    public GameObject lampadaire1, lampadaire2;
    public GameObject[] lampadaires;

    private bool videoStart = false;

    // Use this for initialization

    // Use this for initialization
    void Start()
    {
        //On met tout dans sa situation de départ.
        porteParticules.SetActive(false);

        _gatePosition = _gateObject.transform.position;
        _gateObject.transform.position = _gatePosition;
        foreach (GameObject teleportFloor in _teleportationFloor)
        {
            teleportFloor.SetActive(false);
        }
            
        // On lance le load de toutes les scènes et on affiche un panel de Loading pour prévenir l'utilisateur. Cependant le panel Loading ne s'affiche pas, car le load tourne en async et bouffe tout le CPU.
        m_masterSceneController = MasterSceneController.Instance;
        state = TutoState.LOADING;
        m_masterSceneController.Init();
        ideeMonde1.SetActive(false);
        ideeMonde2.SetActive(false);
        AnimTuto = Image_Tuto.GetComponent<Animator>();
        _tuto.SetActive(false);
        _menu.SetActive(true);
        _tutomove.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (panelLoading.activeSelf) // Si on est dans l'état d'attente
        {
            if (m_masterSceneController.loadingFinished && state == TutoState.LOADING) // Si le loading des scène est terminé
            {
                panelLoading.SetActive(false);
                m_finishInit = true;
                LaunchIntro();
            }

        }
        // On update régulièrement l'état d'avancé du tuto.
        UpdateStates();
        PoolAction();
        GestionUI();

    }

    public void UpdateStates()
    {
        if (videoStart)
        {
            LaunchVideo();
        }
        // On check si l'idée monde numéro 1 est apparu. Pour éviter checker à chaque fois et provoquer des problèmes, on utilise une variable qui track l'event. J'imagine qu'il y a mieux mais j'avais pas d'autre idée.
        if (keepTrackOnIdeeMonde1Appear)
            ideeMonde1Appear = (ideeMonde1.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("End"));

        if (videoIntro.GetComponent<VideoPlayer>().isPlaying && !sourceLogo.isPlaying)
        {
            m_timerLogoAudio += Time.deltaTime;
            if (m_timerLogoAudio >= 2f)
                sourceLogo.Play();
        }

        if (skipTuto) // petite tricks pour saute le tuto (A N'UTILISER QUE POUR DEBUGGER !)
        {
            skipTuto = false;
            EndTuto();
        }
    }

    /// <summary>
    /// Déclenche une liste d'action en fonction de l'état du Tuto
    /// </summary>
    public void PoolAction()
    {
        switch (state)
        {
            case TutoState.MENU:
                CheckMenu();
                break;
            case TutoState.INTRO:
                CheckEndVideo();
                break;
            case TutoState.INTERACTION:
                Interaction();
                break;
            case TutoState.TELEPORTATION:
                break;
        }
    }

    /// <summary>
    /// Lance la vidéo d'intro.
    /// </summary>
    public void LaunchIntro()
    {
        state = TutoState.MENU;
    }

    /// <summary>
    /// Load la scène des neurones.
    /// </summary>
    public void EndTuto()
    {
        m_masterSceneController.LoadScene("MainScene", Color.white);
    }

    public void CheckMenu()
    {
        if (keepTrackOnMenuPlay)
        {
            keepTrackOnMenuPlay = false;
            StartCoroutine(MenuStart());
            
            /*baleine1.GetComponent<AIWaypoints>().playAwake = true;
            baleine2.GetComponent<AIWaypoints>().playAwake = true;*/
        }
        if (keepTrackOnMenuQuit)
        {
            Application.Quit();
        }
        if (keepTrackOnMenuSpeed)
        {
            keepTrackOnMenuSpeed = false;
            StartCoroutine(MenuSkip());
        }
    }

    /// <summary>
    /// Vérifie si la vidéo est terminée.
    /// </summary>
    public void CheckEndVideo()
    {
        // On regarde si la frame actuelle correspond au nombre de frame, si c'est le cas ça veut dire que la vidéo est terminée. 
        if ((ulong)videoIntro.GetComponent<VideoPlayer>().frame == videoIntro.GetComponent<VideoPlayer>().frameCount && (ulong)videoIntro.GetComponent<VideoPlayer>().frame != 0 && !narrativeCont.wait)
        {
            narrativeCont.NextClip();
            StartCoroutine(WaitFade());
        }
    }

    /// <summary>
    /// Gère toute la partie intéraction du tutoriel.
    /// </summary>
    public void Interaction()
    {
        ideeMonde1.SetActive(true);

        ideeMonde1.GetComponent<Animator>().SetTrigger("FadeInGo");

        if (ideeMonde1Appear)
        {
            narrativeCont.NextClip();
            ideeMonde1Appear = false;
            keepTrackOnIdeeMonde1Appear = false;

            Debug.Log("lol");
            // Permet de faire clignoter les triggers
            //hand1.activeTriggerHighlight = true;
            //hand2.activeTriggerHighlight = true;
        }
        if (ideeMonde1.GetComponent<IdeaManager>().activeHand != null && ideeMonde1Grap == false)
        {
            Debug.Log("mdr");
            narrativeCont.NextClip();
            ideeMonde1Grap = true;
            keepTrackOnIdeeMonde1Grap = false;
            ideeMonde2.SetActive(true);
            ideeMonde2.GetComponent<Animator>().SetTrigger("FadeInGo");
        }
        if (ideeMonde2.GetComponent<IdeaManager>().activeHand != null && ideeMonde2Grap == false)
        {
            Debug.Log("ptdr");
            narrativeCont.NextClip();
            ideeMonde2Grap = true;
            keepTrackOnIdeeMonde2Grap = false;
        }

        if (fusionIdea)
        {
            Debug.Log("roflcopter");
            Debug.Log("fusion");
            narrativeCont.NextClip();
            //On détache les sphères pour éviter de détruire les mains avec.

            ideeMonde1.SetActive(false);
            ideeMonde2.SetActive(false);
            _tutomove.SetActive(true);

            // On active la téléportation
            teleportation.SetActive(true);
            fusionIdea = false;
            foreach(GameObject lampadaire in lampadaires)
            {
                if (lampadaire)
                {
                    lampadaire.GetComponent<Lampadaires>().StartMoving();
                }
            }
            //gate.GetComponent<Animator>().SetTrigger("UpGate");
            // hand1.activeTrackPadHighlight = true;
            // hand2.activeTrackPadHighlight = true;
            foreach (GameObject teleportFloor in _teleportationFloor)
            {
                teleportFloor.SetActive(true);
            }
            state = TutoState.TELEPORTATION;
            porteParticules.SetActive(true);
            _marouflage.SetActive(false);
        }
    }

    /// <summary>
    /// Check si l'utilisateur a attrapé une idée monde
    /// </summary>
    /// <param name="indexIdea">Le numéro de l'idée monde. Pour détermine quelle idée monde l'utilisateur à grab</param>
    public void GrapIdea(int indexIdea)
    {
        if (ideeMonde1.GetComponent<IdeaManager>().activeHand != null)
        {
            ideeMonde1Grap = true;
        }
        if (indexIdea == 2 && keepTrackOnIdeeMonde2Grap)
        {
            ideeMonde2Grap = true;
        }
    }

    public void GestionUI()
    {
        //Tuto menu
        if (_menu.activeSelf)
        {
            AnimTuto.SetBool("Anim_TutoMenu", true);
            Image_Tuto.SetActive(true);
            Img_Cercle.SetActive(true);
            
        }

        //Couper le tuto entre le menu et la suite du tuto
        if (_menu.activeSelf == false && state==TutoState.INTRO)
        {
            
            Image_Tuto.SetActive(false);
            Img_Cercle.SetActive(false);
            AnimTuto.SetBool("Anim_TutoMenu", false);
        }

        //Tuto attraper une boule
        if (keepTrackOnIdeeMonde1Appear == false)
        {
            AnimTuto.SetBool("Anim_TutoMenu", false);
            AnimTuto.SetBool("Anim_TutoDrag", true);
            Image_Tuto.SetActive(true);
            Img_Cercle.SetActive(true);
            

        }

        //Tuto fusion
        if (keepTrackOnIdeeMonde2Grap == false)
        {
            
            AnimTuto.SetBool("Anim_TutoDrag", false);
            AnimTuto.SetBool("Anim_TutoFusion", true);
        }

        //Tuto téléportation
        if (_marouflage.activeSelf==false)
        {
            
            AnimTuto.SetBool("Anim_TutoFusion", false);
            AnimTuto.SetBool("Anim_TutoTp", true);
        }
    }

    void LaunchVideo()
    {
        
        videoStart = false;
        videoIntro.GetComponent<VideoPlayer>().Play();
        
        state = TutoState.INTRO;
    }

    IEnumerator MenuStart()
    {
        
        SteamVR_Fade.Start(Color.white, 1);
        yield return new WaitForSeconds(1);
        _menu.SetActive(false);
        _tuto.SetActive(true);
        SteamVR_Fade.Start(Color.clear, 2);
        yield return new WaitForSeconds(2);
        videoStart = true;
    }

    IEnumerator MenuSkip()
    {
        SteamVR_Fade.Start(Color.white, 1);
        yield return new WaitForSeconds(1);
        _menu.SetActive(false);
        _tuto.SetActive(true);
        SteamVR_Fade.Start(Color.clear, 2);
        yield return new WaitForSeconds(2);
        planeVideo.SetActive(false);
        narrativeCont.PassClip();
        state = TutoState.INTERACTION;
        
    }
    // On attend la fin de la voie off avant de faire disparaitre le logo JV.
    IEnumerator WaitFade()
    {
        yield return new WaitForSeconds(narrativeCont.GetClipLenght());
        planeVideo.SetActive(false);
        state = TutoState.INTERACTION;
        Debug.Log("Interaction");
    }

    /** NE SERT PLUS. MAIS JE GARDE AU CAS OU.
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "idea")
        {
            other.GetComponent<IdeaManager>().Respawn();
        }
    } **/

    public void OnDisable()
    {
        Debug.Log("le roi dagobert a mis sa culotte a l'envers");
        foreach (GameObject lampadaire in lampadaires)
        {
            if (lampadaire != null)
            {
                lampadaire.GetComponent<Lampadaires>().Reset();
            }
        }
        narrativeCont.ReturnFirstClip();
        ideeMonde1Appear = false;
        ideeMonde1Grap =false;
        ideeMonde2Grap = false;
        fusionIdea = false;
        _marouflage.SetActive(true);
        porteParticules.SetActive(false);
        keepTrackOnIdeeMonde1Appear = true;
        keepTrackOnIdeeMonde1Grap = true;
        keepTrackOnIdeeMonde2Grap = true;
        _gateObject.transform.position = _gatePosition;
        foreach (GameObject teleportFloor in _teleportationFloor)
        {
            teleportFloor.SetActive(false);
        }
        planeVideo.SetActive(true);
        Img_Cercle.SetActive(false);
        Image_Tuto.SetActive(false);
        _tutomove.SetActive(false);
        _tuto.SetActive(false);
        _menu.SetActive(true);
        state = TutoState.MENU;
        keepTrackOnMenuPlay = false;
        keepTrackOnMenuQuit = false;
        keepTrackOnMenuSpeed = false;
    }

    public void Reloaded(Color fadeOutColor)
    {
        SteamVR_Fade.Start(fadeOutColor, 0);
        SteamVR_Fade.Start(Color.clear, 2);
    }

    public enum TutoState
    {
        LOADING,
        MENU,
        INTRO,
        INTERACTION,
        TELEPORTATION,
    }
}
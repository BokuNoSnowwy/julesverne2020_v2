﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ParticleController : MonoBehaviour {

    public float distanceEclair;
    public GameObject eclair;
    public GameObject eclair2;

    public Transform leftController;
    public Transform rightController;

    

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if ((leftController.childCount == 2 && rightController.childCount == 2) || (leftController.childCount == 6 && rightController.childCount == 6))
        {
            Transform leftChild = leftController;
            Transform rightChild = rightController;
            // Quand on est assez proche, on active le system de particule
            if (Vector3.Distance(leftChild.position,rightChild.position) < distanceEclair)
            {
                // Premier Eclair
                eclair.transform.position = leftChild.position;
                Vector3 direction = eclair.transform.position - rightChild.position;
                eclair.transform.rotation =  Quaternion.LookRotation(direction);
                eclair.SetActive(true);

                //Second Eclair
                eclair2.transform.position = rightChild.position;
                direction = eclair2.transform.position - leftChild.position;
                eclair2.transform.rotation = Quaternion.LookRotation(direction);
                eclair2.SetActive(true);

                            
                leftChild.GetComponent<PulseController>().StartPulse();
                rightChild.GetComponent<PulseController>().StartPulse();


            } else // Si on s'en éloigne
            {
                eclair.SetActive(false);
                eclair2.SetActive(false);
                leftChild.GetComponent<PulseController>().StopPulse();
                rightChild.GetComponent<PulseController>().StopPulse();
            }
        }
        else // Le system ne s'active pas si les idées ne sont pas dans nos mains
        {
            eclair.SetActive(false);
            eclair2.SetActive(false);
            leftController.GetComponent<PulseController>().StopPulse();
            rightController.GetComponent<PulseController>().StopPulse(); 
        }

    }
}

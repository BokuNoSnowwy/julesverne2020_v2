﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateUV : MonoBehaviour {

    public Material mat;

	// Use this for initialization
	void Start () {
        mat = GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        mat.SetTextureOffset("_MainTex", mat.GetTextureOffset("_MainTex") + new Vector2(0.00005f, 0));
	}
}

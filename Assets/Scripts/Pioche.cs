﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class Pioche : MonoBehaviour
{
    public GameObject ps;
    public GameObject player;
    public GameObject hitPoint;
    GameObject wall=null;
    public float offsetter;
    public GameObject target;

    private void OnTriggerEnter(Collider collider)
    {
        target.transform.position = new Vector3(hitPoint.transform.position.x, hitPoint.transform.position.y, target.transform.position.z);
        if(collider.gameObject.tag == "iceWall")
        {
            Vector3 offset = new Vector3(0, 0, offsetter);
            GameObject temp= Instantiate(ps, target.transform.position+offset, Quaternion.identity);
            temp.transform.Rotate(new Vector3(-166.08f, 0, 0));

            Destroy(temp,1.1f);
            
            wall=collider.gameObject;
            wall.SetActive(false);
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.B) && wall != null)
        {
            wall.SetActive(true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteruptorScript : MonoBehaviour
{
    public ParticleSystem particleElevator1;
    public ParticleSystem particleElevator2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ElectricityUp()
    {
        particleElevator1.gameObject.SetActive(true);
        particleElevator2.gameObject.SetActive(true);
    }

    public void ElectricityDown()
    {
        particleElevator1.gameObject.SetActive(false);
        particleElevator2.gameObject.SetActive(false);
    }
}

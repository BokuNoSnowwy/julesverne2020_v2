﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class DecorManagerGrotte : MonoBehaviour, IDecorManager
{
    public string levelToLoad;

    [Header("Light Object")]
    public GameObject[] cristaux;
    public GameObject[] filaments;
    public GameObject[] darkLights;
    public GameObject[] lights;

    [Header("Options")]
    public float speedColor = 1;
    public Color newCristauxColor;
    public Color newFilamentColor;
    public bool allowChangeAmbiance = false;

    [Header("Camera")]
    public int cameraIndex;
    public int cameraPostProcessIndex;
    public Vector3 cameraPosition;
    public Vector3 cameraRotation;

    [Header("Audio")]
    public List<AudioSource> listToPlayOnStart;

    public Teleport teleport;
    public GameObject navMesh;
    public GameObject bestiole;
    public Transform bestioleStart;


    public float timer = 60f;
    private bool sceneIsLoad = false;
    bool countdown = true;

    
    public void InitScene(Color fadeOutColor)
    {
        MasterSceneController.Instance.LoadCamera(cameraIndex, cameraPostProcessIndex, cameraPosition, cameraRotation);
        SteamVR_Fade.Start(fadeOutColor, 0);
        SteamVR_Fade.Start(Color.clear, 2);
        navMesh.SetActive(true);
        sceneIsLoad = false;
        foreach (AudioSource source in listToPlayOnStart)
        {
            source.Play();
        }
        GetComponent<NarrativeController>().RandomClip();
    }

    // Update is called once per frame
    void Update()
    {
        if(countdown)
        timer -= Time.deltaTime;
        if (timer <= 0 && !sceneIsLoad)
        {
            sceneIsLoad = true;
            MasterSceneController.Instance.LoadScene("MainScene", Color.white);
            
        }

        if(allowChangeAmbiance)
        {
            ChangeSceneAmbiance();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            countdown = false;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            timer = -1;
        }
    }

    private void OnDisable()
    {
        sceneIsLoad = false;
        timer = 60;
        bestiole.transform.position = bestioleStart.position;
        bestiole.transform.rotation = bestioleStart.rotation;
        bestiole.GetComponent<AIWaypoints>().numWaypoint = 0;
        bestiole.GetComponent<AIWaypoints>().Restarted();
        countdown = true;
    }
    public void ChangeSceneAmbiance()
    {
        foreach(GameObject cristal in cristaux)
        {
            cristal.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.Lerp(cristal.GetComponent<MeshRenderer>().material.GetColor("_Color"), newCristauxColor, speedColor * Time.deltaTime));
        }

        foreach (GameObject filament in filaments)
        {
            filament.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.Lerp(filament.GetComponent<MeshRenderer>().material.GetColor("_EmissionColor"), newFilamentColor, speedColor * Time.deltaTime));
        }

        foreach (GameObject darklight in darkLights)
        {
            darklight.GetComponent<Light>().color = Color.Lerp(darklight.GetComponent<Light>().color, newCristauxColor, speedColor * Time.deltaTime);
        }

        foreach (GameObject light in lights)
        {
            light.GetComponent<Light>().color = Color.Lerp(light.GetComponent<Light>().color, newFilamentColor, speedColor * Time.deltaTime);
        }
    }
}

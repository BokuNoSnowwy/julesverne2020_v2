﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class DecorManagerGlace : MonoBehaviour, IDecorManager
{
    public string levelToLoad;

    [Header("Camera")]
    public int cameraIndex;
    public int cameraPostProcessIndex;
    public Vector3 cameraPosition;
    public Vector3 cameraRotation;

    [Header("Audio")]
    public List<AudioSource> listToPlayOnStart;

    public Teleport teleport;
    public GameObject navMesh;


    public float timer = 60f;
    private bool sceneIsLoad = false;
    bool countdown = true;

    
    public void InitScene(Color fadeOutColor)
    {
        MasterSceneController.Instance.LoadCamera(cameraIndex, cameraPostProcessIndex, cameraPosition, cameraRotation);
        SteamVR_Fade.Start(fadeOutColor, 0);
        SteamVR_Fade.Start(Color.clear, 2);
        navMesh.SetActive(true);
        sceneIsLoad = false;
        foreach (AudioSource source in listToPlayOnStart)
        {
            source.Play();
        }
        GetComponent<NarrativeController>().RandomClip();
    }

    // Update is called once per frame
    void Update()
    {
        if(countdown)
        timer -= Time.deltaTime;
        if (timer <= 0 && !sceneIsLoad)
        {
            sceneIsLoad = true;
            MasterSceneController.Instance.LoadScene("MainScene", Color.white);
            
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            countdown = false;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            timer = -1;
        }
    }

    private void OnDisable()
    {
        sceneIsLoad = false;
        timer = 60;
        countdown = true;
    }
    
}

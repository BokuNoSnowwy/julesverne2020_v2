﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class DecorManagerMain : MonoBehaviour, IDecorManager {

    [Header("Links")]
    public IdeaGenerator ideaGene;
    public NarrativeController narrativeController;

    [Header("Controllers")]
    public WandController leftController;
    public WandController rightController;

    [Header("Camera Info")]
    public int cameraIndex;
    public Vector3 cameraPosition;
    public Vector3 cameraRotation;

    [Header("End Experience")]
    public AudioSource endSource;
    public Canvas endCanvas;

    [Header("States")]
    public bool sceneIsInit = false;
    public bool endExperience = false;

    public bool scenePassed=false;
    private int time = 0;
    

    void Start()
    {
        MasterSceneController.Instance.GetMain(this);
    }

    public void ResetThis()
    {
        if (ideaGene.listInstanciedIdea.Count > 0)
        {
            foreach(GameObject idea in ideaGene.listInstanciedIdea)
            {
                Destroy(idea);
            }
        }
        ideaGene.Init();
    }

    private void Awake()
    {
        leftController = GameObject.Find("Hand1").GetComponent<WandController>();
        rightController = GameObject.Find("Hand2").GetComponent<WandController>();
    }

    public void InitScene(Color fadeColorOut)
    {
        Debug.Log("Init Scene");
        //MasterSceneController.Instance.LoadCamera(cameraIndex, 0,cameraPosition, cameraRotation);
        SteamVR_Fade.Start(fadeColorOut, 0);
        SteamVR_Fade.Start(Color.clear, 2);

        
        if(scenePassed==false)
        {
            ideaGene.Init();
            narrativeController.NextClip();
            sceneIsInit = true;
        }
        
        Debug.Log("main liste " + ideaGene.listInstanciedIdea.Count);

        if(ideaGene.listInstanciedIdea.Count <= 0 && scenePassed==true) // Fin de l'expérience
        {
            Debug.Log("EndStart");
            endSource.Play();
            endExperience = true;
        } 
    }

    public void Update()
    {
        if(sceneIsInit)
        {
            if (scenePassed == false && (leftController.haveObject || rightController.haveObject))
            {
                scenePassed = true;
                narrativeController.NextClip();
            }
        }
       
        if(endExperience /*&& !endSource.isPlaying*/)
        {
            endCanvas.worldCamera = Camera.main;

            if (time < endSource.clip.length*100)
            {
                time++;
            }
            else
            {
                time = 0;
                endExperience = false;
                scenePassed = false;
                endCanvas.worldCamera = null;
                //endCanvas.GetComponent<Animator>().SetTrigger("FadeIn");
                Debug.Log("EndEnd");
                MasterSceneController.Instance.LoadScene("SceneTuto copied", Color.black);
            }
            
        }
    }

    private void OnDisable()
    {
        ideaGene.EnableIdeas();
    }
}

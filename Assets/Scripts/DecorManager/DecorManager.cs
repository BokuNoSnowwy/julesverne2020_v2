﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// J'utilise une Interface pour pouvoir appeler une fonction qui va substituer Start(). Il est nécessaire que chaque scène implémente un DecorManager avec la fonction InitScene().
/// </summary>
public interface IDecorManager {

    void InitScene(Color fadeOutColor);
}

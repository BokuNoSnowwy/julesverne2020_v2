﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class DecorManagerDesert : MonoBehaviour, IDecorManager {

    [Header("Debug")]
    public bool debugMode = false;

    [Header("Camera")]
    public int cameraIndex;
    public int cameraPostProcessIndex;
    public Vector3 cameraPosition;
    public Vector3 cameraRotation;
    public Transform baleineStart;

    [Header("Events")]
    public GameObject sandStorm;
    public GameObject baleine;
    public GameObject player;
    //public List<SpotTarget> spots;

    [Header("Audio")]
    public AudioSource baleineSource;
    public AudioSource tempestSource;
    public List<AudioSource> listToPlayOnStart;

    [Header("AI Waypoint")]
    public List<AIWaypoints> drones;
    public Transform[] dronesStart;

    public Teleport teleport;

    public float timer = 120f;
    private bool sceneIsLoad = false;
    bool countdown = true;


    private bool _baleine = false; // Si on a activé la baleine

    // Use this for initialization
    void Start () {
        if(debugMode)
            InitScene(Color.black);
        for(int i=0; i<drones.Count; i++)
        {
            
            dronesStart[i].position = drones[i].gameObject.transform.position;
            dronesStart[i].rotation= drones[i].gameObject.transform.rotation;
        }
        baleineStart.position = baleine.transform.position;
        baleineStart.rotation = baleine.transform.rotation;
    }

    public void InitScene(Color fadeOutColor)
    {
        //a inité pour démarrer la scene
        if(!debugMode)
        {
            MasterSceneController.Instance.LoadCamera(cameraIndex, cameraPostProcessIndex, cameraPosition, cameraRotation);
            SteamVR_Fade.Start(fadeOutColor, 0);
            SteamVR_Fade.Start(Color.clear, 2);
            
        }
        sceneIsLoad = false;
        //teleport.Init();
        InitDrones();
        GetComponent<NarrativeController>().RandomClip();
        foreach(AudioSource source in listToPlayOnStart)
        {
            source.Play();
        }
        Invoke("LaunchBaleine", 15);
        
    }

    public void InitDrones()
    {
        foreach(AIWaypoints drone in drones)
        {
            drone.playAwake = true;
        }
    }


    

    void LaunchBaleine()
    {
        /*foreach(SpotTarget spot in spots)
        {
            spot.ActiveSpot();
        }*/
        baleineSource.Play();
        baleine.GetComponent<AIWaypoints>().playAwake = true;
        baleine.GetComponent<Animator>().SetTrigger("Move");
        _baleine = true;
       // StartCoroutine(moveDustStorm());
        
    }

    /*IEnumerator moveDustStorm()
    {
        while (Vector3.Distance(sandStorm.transform.position, player.transform.position) > 0.1f)
        {
            sandStorm.transform.position = Vector3.MoveTowards(sandStorm.transform.position, player.transform.position, 17 * Time.deltaTime);
            yield return null;
        }
    }*/

    // Update is called once per frame
    void Update () {

        if (countdown)
            timer -= Time.deltaTime;
        if (timer <= 0 && !sceneIsLoad && !debugMode)
        {
            sceneIsLoad = true;
            MasterSceneController.Instance.LoadScene("MainScene", Color.white);
            
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            countdown = false;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            timer = -1;
        }
    }

    private void OnDisable()
    {
        /*foreach (SpotTarget spot in spots)
        {
            spot.UnactiveSpot();
        }*/
        baleine.GetComponent<AIWaypoints>().playAwake = false;
        baleine.transform.position = baleineStart.position;
        baleine.transform.rotation = baleineStart.rotation;
        _baleine = false;
        baleine.GetComponent<AIWaypoints>().numWaypoint = 0;
        baleine.GetComponent<AIWaypoints>().Restarted();
        sceneIsLoad = false;
        timer = 120;
        foreach (AIWaypoints drone in drones)
        {
            drone.playAwake = false;
            drone.numWaypoint = 0;
            drone.Restarted();
        }
        for (int i = 0; i < drones.Count; i++)
        {
             drones[i].gameObject.transform.position = dronesStart[i].position;
             drones[i].gameObject.transform.rotation = dronesStart[i].rotation;
        }
        countdown = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Valve.VR;

public class IdeaManager : MonoBehaviour
{

    public IdeaGenerator ideagen;

    public WandController activeHand = null;

    public string sceneToLoad; // La Scene à charger

    public GameObject explodeParticle;

    public TutoController tutoCont;

    public float speed; // La vitesse de déplacement de l'idée
    public float radiusMove; // Le rayon de la sphere dans laquelle peut se déplacer l'idée

    private IEnumerator _move; // La coroutine qui gère le déplacement de l'idée

    private Vector3 _moveAround; // Position de départ de la sphere

    private Vector3 _destination; //Mouvement des Idées

    private void OnEnable()
    {
        Init();
    }

    // Use this for initialization
    public void Init()
    {
        _moveAround = transform.position;
        InitMove();
    }

    public void GetDisable()
    {
        if (ideagen.disabled == false)
        {
            ideagen.DisableIdeas(sceneToLoad);
        }
    }

    public void InitMove()
    {
        _move = IdeaMove();
        StartCoroutine(_move);
    }

    void OnTriggerEnter(Collider idea)
    {
        Debug.Log("Collision");
        // Check si l'idée peut fusionner avec l'objet en collision ou si les deux idées sont grab
        if (idea.tag == "idea" && idea.GetComponent<IdeaManager>().activeHand!=null && activeHand!=null)
        {
            Debug.Log("Idée peut fusionner ! ");
            if (sceneToLoad != "" && idea.GetComponent<IdeaManager>().sceneToLoad==sceneToLoad)
            {
                GameObject.Find("Root/IdeeGenerator").GetComponent<IdeaGenerator>().RemoveIdeas(idea.gameObject, gameObject);
                // On supprime les idées de la liste de génération 
                Destroy(Instantiate(explodeParticle, Vector3.Lerp(transform.position, idea.transform.position,0.5f),Quaternion.identity),3);
                MasterSceneController.Instance.LoadScene(sceneToLoad, Color.black);
                Destroy(gameObject);
                Destroy(idea.gameObject);
            }
            else if(tutoCont)
            {
                Debug.Log("OK Pierre ");
                Destroy(Instantiate(explodeParticle, Vector3.Lerp(transform.position, idea.transform.position, 0.5f), Quaternion.identity),3);
                tutoCont.fusionIdea = true;
            }
            else
            {
                Destroy(Instantiate(explodeParticle, Vector3.Lerp(transform.position, idea.transform.position, 0.5f), Quaternion.identity),3);
            }
        }
    }

    //Déplacement aléatoire des idées
    IEnumerator IdeaMove() 
    {
        while (gameObject.activeSelf)
        {
            // Sphere de déplacement depuis la position de départ de l'idée
            _destination = _moveAround + Random.insideUnitSphere * radiusMove;

            while (Vector3.Distance(transform.position, _destination) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, _destination, speed * Time.deltaTime);

                yield return null;
            }
        }
        yield return null;
    }

    // Active le déplacement des idées
    public void StartMove()
    {
        if(gameObject.activeSelf)
            StartCoroutine(_move);
    }

    public void Respawn()
    {
        transform.position = _moveAround;
        GetComponent<Rigidbody>().isKinematic = true;
    }

    // Désactive le déplacement des idées
    public void StopMove()
    {
        StopCoroutine(_move);
    }

    // Interaction physique avec les objets
    public void ThrowObjet(SteamVR_Action_Pose controller)
    {
        GetComponent<Rigidbody>().velocity = controller.velocity;
        GetComponent<Rigidbody>().angularVelocity = controller.angularVelocity;
    }
}

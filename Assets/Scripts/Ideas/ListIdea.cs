﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListIdea : MonoBehaviour {

    public List<GameObject> listIdea;
    public static ListIdea instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(instance);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

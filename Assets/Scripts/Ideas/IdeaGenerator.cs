﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IdeaGenerator : MonoBehaviour {

    public List<GameObject> listInstanciedIdea;

    public List<Vector3> listPos;
    public List<Vector3> listPosInfinite;
    public bool disabled=false;

    // Use this for initialization
    public void Start()
    {
        for(int i=0; i<listPos.Count; i++)
        {
            listPosInfinite.Add(listPos[i]);
        }
    }

    public void Init()
	{
                int index;
		List<GameObject> listIdea = GameObject.Find("MasterSceneController").GetComponent<MasterSceneController>().listIdea;
        foreach (GameObject idea in listIdea)
		{
            index = Random.Range(0, listPos.Count);
			GameObject ideaObj = Instantiate(idea, listPos[index], Quaternion.identity, transform.parent);
            ideaObj.GetComponent<IdeaManager>().ideagen = this;
            ideaObj.name = idea.name;
            ideaObj.GetComponent<IdeaManager>().Init();
            listInstanciedIdea.Add(ideaObj);
            listPos.RemoveAt(index);
		}
        for (int i = 0; i < listPosInfinite.Count; i++)
        {
            listPos.Add(listPosInfinite[i]);
        }
    }

    /*public void MoveIdea()
    {
        foreach (GameObject idea in listInstanciedIdea)
        {
            idea.GetComponent<IdeaManager>().InitMove();
        }
    }*/

    public void DisableIdeas(string sceneName)
    {

        foreach (GameObject idea in listInstanciedIdea)
        {
            if(idea.GetComponent<IdeaManager>().sceneToLoad != sceneName)
            {
                idea.SetActive(false);
            }
        }
        disabled = true;
    }
    public void EnableIdeas()
    {
        foreach (GameObject idea in listInstanciedIdea)
        {
                idea.SetActive(true);
        }
        disabled = false;
    }

    public void RemoveIdeas(GameObject idea1, GameObject idea2)
    {
        listInstanciedIdea.RemoveAll(idea => idea.name == idea1.name || idea.name == idea2.name);
        Debug.Log("liste " + listInstanciedIdea.Count);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Gère toute la partie interaction avec les idées mondes. Permet un meilleur contrôle des différents cas d'intéraction.
/// </summary>
public class InteractionSphere : MonoBehaviour {

    public WandController rightController;
    public WandController leftController;

    // La liste de toutes les fusions possibles
    public Fusion[] fusions;

    /// <summary>
    /// Check si le controller peut prendre la sphere ou non.
    /// </summary>
    /// <param name="collisionSPhere">La sphere avec laquelle le controller vient d'entrer en contact</param>
    /// <returns></returns>
    public bool ControllerTakeSphere (GameObject collisionSPhere)
    {
        Debug.Log("rightControllerhaveobject : " + rightController.haveObject);
        Debug.Log("leftControllerhaveobject : " + leftController.haveObject);
        if (!rightController.haveObject && !leftController.haveObject) // Si aucun des controllers n'a d'idée monde en main, pas de soucis.
        {
            return true;
        }
        else // Si l'un des controllers a une idée monde en main.
        {
            Debug.Log("A quelque chose dans les mains");
            if (rightController.haveObject || leftController.haveObject)
            {
                // On regarde quel controller à une idée monde en main et on la récupère.
                IdeaManager idea = (rightController._pickup != null) ? rightController._pickup.GetComponent<IdeaManager>() : leftController._pickup.GetComponent<IdeaManager>();
                Debug.Log("Idea name :" + idea.name);
                Debug.Log("Sphere name :" + collisionSPhere.name);
                foreach (Fusion fusionIdea in fusions)
                {
                    // On parcour la liste des fusions possible et on regarde si elles peuvent fusionnées.
                    if (fusionIdea.FusionOkay(idea) && fusionIdea.FusionOkay(collisionSPhere.GetComponent<IdeaManager>()))
                    {
                        Debug.Log("Fusion ok !");
                        return true;
                    }
                }
            }
            Debug.Log("Fusion pas OK !");
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="controller"></param>
    /// <param name="collisionSPhere"></param>
    /// <returns></returns>
    public bool AlreadyHaveOne (WandController controller, GameObject collisionSPhere)
    {
        if (rightController == controller)
        {
            return (collisionSPhere == leftController._pickup);
        }
        
        return (collisionSPhere == rightController._pickup);
    }

    public void SwapController (WandController controller, GameObject collisionSPhere)
    {
        Debug.Log("swap");
        if (rightController == controller && AlreadyHaveOne(rightController, collisionSPhere))
        {
            leftController._pickup = null;
            leftController.haveObject = false;
            Debug.Log("right");
        }
        else if (leftController == controller && AlreadyHaveOne(leftController, collisionSPhere))
        {
            rightController._pickup = null;
            rightController.haveObject = false;
            Debug.Log("left");
        }
    }
}

[System.Serializable]
public class Fusion
{
    public IdeaManager idea1, idea2;

    public bool FusionOkay(IdeaManager idea)
    {
        if (idea.name == idea1.name || idea.name == idea2.name)
        {
            return true;
        }
        return false;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StockGrab : MonoBehaviour {

    private GameObject _objToGrab = null;

    // Change l'objet que l'on peut grab
    public void SetObjToGrab(GameObject obj)
    {
        _objToGrab = obj;
    }

    // Retourne l'objet que l'on peu grab
    public GameObject GetObjToGrab()
    {
        return _objToGrab;
    }
}

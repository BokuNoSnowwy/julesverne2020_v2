﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class IdeaMove : MonoBehaviour {

    public GameObject explodeParticle;

    public float speed; // La vitesse de déplacement de l'idée
    public float radiusMove; // Le rayon de la sphere dans laquelle peut se déplacer l'idée

    private IEnumerator _move; // La coroutine qui gère le déplacement de l'idée

    private Vector3 _moveAround; // Position de départ de la sphere

    private Vector3 _destination; //Mouvement des Idées


    // Use this for initialization
    void Start()
    {
        _moveAround = transform.position;
        _move = IdeaMoving();
        StartCoroutine(_move);

    }

    //Déplacement aléatoire des idées
    IEnumerator IdeaMoving()
    {
        while (true)
        {
            // Sphere de déplacement depuis la position de départ de l'idée
            _destination = _moveAround + Random.insideUnitSphere * radiusMove;

            while (Vector3.Distance(transform.position, _destination) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, _destination, speed * Time.deltaTime);
                yield return null;

            }
        }


    }

    // Active le déplacement des idées
    public void StartMove()
    {
        StartCoroutine(_move);
    }

    // Désactive le déplacement des idées
    public void StopMove()
    {
        StopCoroutine(_move);
    }

    // Interaction physique avec les objets
    public void ThrowObjet(SteamVR_Action_Pose controller)
    {
        GetComponent<Rigidbody>().velocity = controller.velocity;
        GetComponent<Rigidbody>().angularVelocity = controller.angularVelocity;
    }

    // Active ou désactive l'émission du matériel de l'idée
    public void EmissiveMaterial(bool isEmissive)
    {
        if (isEmissive)
        {
            GetComponent<Material>().EnableKeyword("_EMISSION");
            GetComponent<Material>().SetColor("_EmissionColor", new Color(0.4f, 0.8f, 1));
        }
        else
        {
            GetComponent<Material>().DisableKeyword("_EMISSION");
        }
    }
}

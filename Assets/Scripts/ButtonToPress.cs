﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonToPress : MonoBehaviour
{

    public int indexButton;
    public ElevatorBehavior elevator;

    public bool pressed = false;

    void Start()
    {

    }


    void Update()
    {
        
    }

    public void CallElevatorToPlayer()
    {
        Debug.Log("Touche");
        if(!pressed)  
        {
            elevator.CallElevator(indexButton);
            pressed = true;
            StartCoroutine("WaitToDepush");
        }
    }

    public void OnMouseDown()
    {
        if (!pressed)
        {
            elevator.CallElevator(indexButton);
            pressed = true;
            StartCoroutine("WaitToDepush");
        }
    }

    IEnumerator WaitToDepush()
    {
        yield return new WaitForSeconds(2f);
        pressed = false;
    }
   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ElevatorBehavior : MonoBehaviour
{
    public GameObject firstPoint;
    public GameObject secondPoint;
    public GameObject player;
    public GameObject miniSphinx = null;

    public float speed = 5f;

    public bool enable = false;
    public bool isUp = true;
    public bool isMovingDown = false;
    public bool isMovingUp = false;
    public bool isMovingForward = false;
    public bool isMovingBackward = false;
    private bool isMoving = false;
    public float heighted;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enable)
        {
            //L'ascenseur va monter ou descendre en fonction des booléens et s'arreter aux étages correspondants
            if (isMovingDown)
            {
                //Vérifie si il a atteint l'étage + ajustement par de l'ascenseur par rapport a l'épaisseur de l'étage
                if (transform.position.y <= firstPoint.gameObject.transform.position.y + (firstPoint.gameObject.transform.localScale.y / 2))
                {
                    isMovingDown = false;
                    isUp = false;
                    isMoving = false;
                }
                else
                {
                    //Déplacement
                    isMoving = true;
                    transform.Translate(Vector3.down * Time.deltaTime * speed);

                }
            }
            else if (isMovingUp)
            {
                //Vérifie si il a atteint l'étage + ajustement par de l'ascenseur par rapport a l'épaisseur de l'étage
                if (transform.position.y >= secondPoint.gameObject.transform.position.y + (secondPoint.gameObject.transform.localScale.y / 2))
                {
                    isMovingUp = false;
                    isUp = true;
                    isMoving = false;
                }
                else
                {
                    //Déplacement
                    isMoving = true;
                    transform.Translate(Vector3.up * Time.deltaTime * speed);

                }
            }
            else if (isMovingForward)
            {
                //Vérifie si il a atteint l'étage + ajustement par de l'ascenseur par rapport a l'épaisseur de l'étage
                if (transform.position.z >= secondPoint.gameObject.transform.position.z)
                {
                    isMovingForward = false;
                    isMoving = false;
                }
                else
                {
                    //Déplacement du train vers sa destination
                    isMoving = true;
                    transform.position = Vector3.MoveTowards(transform.position, secondPoint.transform.position, speed * Time.deltaTime);
                }
            }

            else if (isMovingBackward)
            {
                //Vérifie si il a atteint l'étage + ajustement par de l'ascenseur par rapport a l'épaisseur de l'étage
                if (transform.position.z <= firstPoint.gameObject.transform.position.z)
                {
                    isMovingBackward = false;

                }
                else
                {
                    //Déplacement
                    isMoving = true;
                    transform.position = Vector3.MoveTowards(transform.position, firstPoint.transform.position, speed * Time.deltaTime);
                }
            }

            if (player != null)
            {
                if (isMoving)
                {
                    player.transform.position = new Vector3(transform.position.x, transform.position.y + heighted, transform.position.z);
                }
            }    
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag=="Player")
        {
            Debug.Log("EnterPlayer");
            player = other.gameObject;
            heighted = player.transform.position.y - transform.position.y;
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("ExitPlayer");
            player = null;
        }
    }


    public void CallElevator(int indexButton)
    {
        if (enable)
        {
            Debug.Log("Calling");
            switch (indexButton)
            {
                case 1:
                    if (isUp)
                    {
                        isMovingDown = true;
                    }
                    break;
                case 2:
                    if (!isUp)
                    {
                        isMovingUp = true;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void EnableElevator()
    {
        if (!enable)
        {
            Debug.Log("Enabled");
            enable = true;
        }
    }

    public void DisableElevator()
    {
        if (enable)
        {
            Debug.Log("Disabled");
            enable = false;
        }
    }

    //Fait avancer le train (fonction appelée depuis le petit sphinx)
    public void MoveForward()
    {
        if (isMovingBackward)
        {
            isMovingBackward = false;
        }
        Debug.Log("Forward");
        isMovingForward = true;
    }

    //Fait reculer le train (fonction appelée depuis le petit sphinx)
    public void MoveBackward()
    {
        if (isMovingForward)
        {
            isMovingForward = false;
        }
        Debug.Log("Backward");
        isMovingBackward = true;

    }
}

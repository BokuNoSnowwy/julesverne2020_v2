﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITarget : MonoBehaviour {

    public GameObject target;
    public GameObject headTarget;
    public float speed;
    public float angularSpeed;

    private Vector3 _oldTargetPos;
    private Vector3 _targetPos;
    private Quaternion randomAngle;

	// Use this for initialization
	void Start () {
        randomAngle = Quaternion.Euler(transform.rotation.x, transform.rotation.y + Random.Range(-180, 180), transform.rotation.z);
        Debug.Log(randomAngle.y);
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 direction;
        _targetPos = new Vector3(target.transform.position.x, headTarget.transform.position.y, target.transform.position.z);
        if (Vector3.Distance(transform.position,_targetPos) > 4f)
        {
            transform.position = Vector3.MoveTowards(transform.position, _targetPos, speed * Time.deltaTime);
            direction = _targetPos - transform.position;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), angularSpeed * Time.deltaTime);
        }
        else
        {
            if(Quaternion.Angle(transform.rotation, randomAngle) > 0.5f)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, randomAngle, angularSpeed * Time.deltaTime);
            }
            else
            {
                randomAngle = Quaternion.Euler(transform.rotation.x, transform.rotation.y + Random.Range(-180, 180), transform.rotation.z);
            }
           // transform.position += Random.Range(0.1f, 0.3f) * (Mathf.Sin(2 * Mathf.PI * 0.05f * Time.time) - Mathf.Sin(2 * Mathf.PI * 0.05f * (Time.time - Time.deltaTime))) * transform.up;
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotTarget : MonoBehaviour {

    public Transform target;

    public Transform support;
    public Transform spot;

    public GameObject spotLight;
    public GameObject pointLight;

    public AudioSource source;

    public float speed = 1;

    private bool m_spotActive = false;

	public void ActiveSpot()
    {
        spotLight.SetActive(true);
        pointLight.SetActive(true);

        m_spotActive = true;
    }

    public void UnactiveSpot()
    {
        spotLight.SetActive(false);
        pointLight.SetActive(false);

        m_spotActive = false;
    }

    private void Update()
    {
        if(m_spotActive)
        {

            source.Play();

            Vector3 lookPosition = support.position - target.position;
            lookPosition.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPosition);
            support.rotation = Quaternion.Slerp(support.rotation, rotation, speed * Time.deltaTime);

            Vector3 lookPosition2 = spot.localPosition - target.position;
            lookPosition2.x = 0;
            Quaternion rotation2 = Quaternion.LookRotation(lookPosition2);
            spot.localRotation = Quaternion.Slerp(spot.localRotation, rotation2, speed * 0.05f * Time.deltaTime);
            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {

    public TutoController tutoCont;
    public AudioSource GateSound;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GateSound.Play();
            tutoCont.EndTuto();
        }
    }
}

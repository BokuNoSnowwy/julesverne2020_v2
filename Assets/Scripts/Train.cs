﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;




public class Train : MonoBehaviour
{
    public AIWaypoints aiw;
    public GameObject joueur = null;
    public Vector3 lastPos;
    public Vector3 offset;
    private void Start()
    {
        aiw = GetComponent<AIWaypoints>();
        aiw.enabled = false;
        lastPos = transform.position;
    }

    private void Update()
    {
        if (joueur == null)
        {
            aiw.enabled = false;
        }
        else
        {
            offset = transform.position - lastPos;
            joueur.transform.Translate(offset);
            lastPos = transform.position;
        }
        
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            aiw.enabled = true;
            joueur = collider.gameObject;
            offset = joueur.transform.position - transform.position;
        }
            
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            aiw.enabled = false;
            joueur = null;
        }
    }
}

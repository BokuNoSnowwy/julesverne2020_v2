﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWaypoints : MonoBehaviour {

    public GameObject[] allWaypoints;

    public float speed;
    public float angularSpeed;
    
    public bool playAwake = true;

    private Vector3 _destination;
    private Vector3 _direction;

    public int numWaypoint = 0;

	// Use this for initialization
    // invoke le mouvement ici plutôt que dans Update
	 void Start () {
        _destination = allWaypoints[numWaypoint].transform.position;
        _direction = _destination - transform.position;
    }

    public void Restarted()
    {
        _destination = allWaypoints[numWaypoint].transform.position;
        _direction = _destination - transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if(playAwake)
        {
            MovingWhale();
        }
	}

    void MovingWhale()
    {
        if (numWaypoint < allWaypoints.Length && playAwake)
        {
            if (Vector3.Distance(transform.position, _destination) > 0.1f)
            {
                //transform.position += Random.Range(0.2f,0.5f) *( ((Mathf.Sin(2 * Mathf.PI * 0.2f * Time.time) - Mathf.Sin(2 * Mathf.PI * 0.2f * (Time.time - Time.deltaTime))) * transform.up) + ((Mathf.Cos(2 * Mathf.PI * 0.2f * Time.time) - Mathf.Cos(2 * Mathf.PI * 0.2f * (Time.time - Time.deltaTime))) * transform.right));
                
                if (gameObject.tag == "papillon")
                {
                    transform.position = Vector3.MoveTowards(transform.position, _destination, speed * Time.deltaTime);
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_destination), angularSpeed * Time.deltaTime);
                   // GetComponentInChildren<Transform>().rotation= Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_destination), angularSpeed * Time.deltaTime);
                }

                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, _destination, speed * Time.deltaTime);
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_direction), angularSpeed * Time.deltaTime);
                }
            }
            else
            {
                if (numWaypoint + 1 < allWaypoints.Length)
                {
                    ++numWaypoint;
                    _destination = allWaypoints[numWaypoint].transform.position;
                    _direction = _destination - transform.position;
                }
                else
                {
                    numWaypoint = 0;
                    _destination = allWaypoints[numWaypoint].transform.position;
                    _direction = _destination - transform.position;
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIRandom : MonoBehaviour {

    public float speed;
    public float angularSpeed;
    public float tankSize;

    public bool playAwake = true;
    public bool grab = false;

    private bool _collideSol = false;
    private bool _arrived = false;

    private Vector3 _destination;
    private Vector3 _direction;
    private Vector3 _startPos;

    // Use this for initialization
    void Start()
    {
        _startPos = transform.position;
        _destination = new Vector3(Random.Range(-tankSize, tankSize) + _startPos.x, transform.position.y, Random.Range(-tankSize, tankSize) + _startPos.z);
        _direction = _destination - transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, _destination) > 0.1f && !_arrived)
        {
            transform.position += Random.Range(0.1f, 0.2f) * ((Mathf.Sin(2 * Mathf.PI * 0.2f * Time.time) - Mathf.Sin(2 * Mathf.PI * 0.2f * (Time.time - Time.deltaTime))) * transform.up);
            transform.position = Vector3.MoveTowards(transform.position, _destination, speed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_direction), angularSpeed * Time.deltaTime);
        }
        else
        {
            _arrived = true;
            if(grab)
            {
                if(!_collideSol)
                {
                    transform.Translate(Vector3.down * Time.deltaTime * speed);
                    transform.Rotate(Vector3.right * angularSpeed * 5 * Time.deltaTime);
                }
                else
                {
                    GetComponent<Animator>().SetBool("isStop", true);
                    if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("idle"))
                    {
                        _destination = new Vector3(Random.Range(-tankSize, tankSize) + _startPos.x,_startPos.y, Random.Range(-tankSize, tankSize) + _startPos.z);
                        _direction = _destination - transform.position;
                        GetComponent<Animator>().SetBool("isStop", false);
                        _collideSol = false;
                        _arrived = false;
                    }
                }
            }
            else
            {

                _destination = new Vector3(Random.Range(-tankSize, tankSize) + _startPos.x, transform.position.y, Random.Range(-tankSize, tankSize) + _startPos.z);
                _direction = _destination - transform.position;
                _arrived = false;
            }
            
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.collider.tag == "sol" && _arrived)
        {
            Debug.Log("Collision");
           _collideSol = true;
        }
    }
}

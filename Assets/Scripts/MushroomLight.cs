﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class MushroomLight : MonoBehaviour {

    public DecorManagerGrotte decorGrotte;

    public Color cristauxColor;

    [ColorUsageAttribute(true, true)]
    public Color filamentColor;

    public AudioSource sourceAudio;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Shroom()
    {
        Debug.Log("Trigger Mushroom");
        
            GetComponent<PlayableDirector>().Play();
            decorGrotte.newCristauxColor = cristauxColor;
            decorGrotte.newFilamentColor = filamentColor;
            decorGrotte.allowChangeAmbiance = true;
            sourceAudio.Play();
    }
}

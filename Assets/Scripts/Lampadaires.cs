﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lampadaires : MonoBehaviour
{
    
    /// Permet de faire sortir les lampadaires et la porte du sol

    //public GameObject previousLamp;
    public bool moved=false;
    public float maxHeight;
    public float speed;
    bool endUp=false;
    public float verticalSpeed;
    public float floatLimit;
    float speedUp;
    float speedDown;
    bool movingUp;
    public bool isFloating;
    Vector3 startPos;


    private void Start()
    {
        speedUp = verticalSpeed;
        speedDown = -verticalSpeed;
        startPos = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        /*if (previousLamp != null){
            if (previousLamp.transform.position.y >= previousLamp.GetComponent<Lampadaires>().maxHeight && moved == false)
            {
                StartMoving();
            }
        }*/
        
        //Dés que move est activé, on initie la montée jusqu'à la hauteur définie.
        if(moved==true && transform.position.y < maxHeight)
        {
            transform.Translate(new Vector3(0, speed, 0) * Time.deltaTime, Space.World);
        }
        if(transform.position.y >= maxHeight && endUp==false)
        {
            endUp = true;
            moved = false;
            movingUp = true;
        }

        if (endUp == true && isFloating)
        {
            transform.Translate(new Vector3(0, verticalSpeed, 0) * Time.deltaTime, Space.World);
            if (transform.position.y >= maxHeight+floatLimit)
            {
                verticalSpeed = speedDown;
            }
            if (transform.position.y <= maxHeight - floatLimit)
            {
                verticalSpeed = speedUp;
            }
        } 
    }

    public void Reset()
    {
        moved = false;
        endUp = false;
        transform.position = startPos;
    }
    //Couper le mouvement. A faire au reset pour les empêcher de monter dés le début.
    public void StopMoving()
    {
        moved = false;
    }

    //Activer le mouvement
    public void StartMoving()
    {
        moved = true;
    }
}

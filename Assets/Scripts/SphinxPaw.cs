﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphinxPaw : MonoBehaviour
{
    public string paw;
    public bool activated;
    public ElevatorBehavior train;

    //Quand le joueur touche une des pattes du sphinx, il y a différentes intéractions
    public void TouchPaw()
    {
        //Touche la patte gauche du sphinx, le train démarre 
        if(paw == "left")
        {
            if (!activated)
            {
                Debug.Log("TrainOn");
                train.EnableElevator();
                activated = true;
            }
            else
            {
                Debug.Log("TrainOff");
                train.DisableElevator();
                activated = false;
            }
            
        
        }
        //Touche la patte droite du sphinx, les lumières s'allument, s'éteignent
        else if(paw == "right")
        {
            if (!activated)
            {
                Debug.Log("LightOn");
                activated = true;
            }
            else
            {
                Debug.Log("LightOff");
                activated = false;
            }
        }
    }

    public void TravelToSphinx()
    {
        train.MoveForward();
    }

    public void TravelToCity()
    {
        train.MoveBackward();
    }
}
